# **Appro-app modules : CoreData**
## (Module not finished yet on my side. Don't pay attention to it's comments, annotations, etc.)



Table:

* SETUP





### **SETUP**

Add the following functions in the AppDelegate:

```swift
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?  lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "CUSTOM_NAME") // Add your container name 
    container.loadPersistentStores(completionHandler: {
     (storeDescription, error) in
      print(storeDescription)
    
      if let error = error as NSError? {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    })   
    return container
  }()  
  
  func saveContext() {
    let context = persistentContainer.viewContext    
    if context.hasChanges {
      do {
        try context.save()
      } catch {
        let err = error as NSError
        fatalError("Unresolved error \(err), \(err.userInfo)")
      }
    }
  }
```

### 
