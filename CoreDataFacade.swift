//
//  CoreDataFacade.swift
//  Appro-app Modules
//
//  Created by Matt Jacquet on 4/26/21.
//

import UIKit
import CoreData

protocol CoreDataModule {
    func getManagedObject(forEntityName: String, completion: @escaping(CoreDataErrorHandler? , NSManagedObjectContext?, NSManagedObject?) -> ())
    func saveManagedObject(context: NSManagedObjectContext)
    func addValueToManagedObject(managedObject: NSManagedObject, key: String, value: Any)
    func fetch(entityName: String) throws -> (NSManagedObjectContext, [NSManagedObject])
    func getValueFromKeyInEntity(key: String, managedObject: NSManagedObject) -> Any?
    func createNewMAnagedObject(entityName: String, context: NSManagedObjectContext) -> NSManagedObject?
}

// MARK: Public Container
struct CoreDataModuleContainer {
    static func getModule(mock: Bool) -> CoreDataModule {
#if DEBUG
    return mock ? CoreDataMock() : CoreDataFacade()
#else
    return CoreDataFacade()
#endif
    }
}

// MARK: Facade
private class CoreDataFacade: CoreDataModule {
    
    func getManagedObject(forEntityName: String, completion: @escaping(CoreDataErrorHandler?, NSManagedObjectContext?, NSManagedObject?) -> ()) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        if let entity = NSEntityDescription.entity(forEntityName: forEntityName, in: context) {
            let managedObject = NSManagedObject(entity: entity, insertInto: context)
            completion(nil, context, managedObject)
        }else {
            completion( CoreDataErrorHandler(errorCode: .ENTITY_NOT_FOUND, description: nil), nil, nil)
        }
    }
    
    func createNewMAnagedObject(entityName: String, context: NSManagedObjectContext) -> NSManagedObject? {
        if let entity = NSEntityDescription.entity(forEntityName: entityName, in: context) {
            let managedObject = NSManagedObject(entity: entity, insertInto: context)
            return managedObject
        }
        return nil
    }
    
    func saveManagedObject(context: NSManagedObjectContext) {
        do {
            try context.save()
        } catch {
            print("Failed saving:", error.localizedDescription)
        }
    }
    
    func addValueToManagedObject(managedObject: NSManagedObject, key: String, value: Any) {
        managedObject.setValue(value, forKey: key)
        
    }
    
    func fetch(entityName: String) throws -> (NSManagedObjectContext, [NSManagedObject]) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            return (context, result)
        } catch let error {
            throw CoreDataErrorHandler(errorCode: .CANNOT_FETCH_DATA , description: error.localizedDescription)
        }
    }
    
    func getValueFromKeyInEntity(key: String, managedObject: NSManagedObject) -> Any? { return managedObject.value(forKey: key) }
    
    func remove(entityName: String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let requestDel = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        requestDel.returnsObjectsAsFaults = false
        // If you want to delete data on basis of some condition then you can use NSPredicate
        //  let predicateDel = NSPredicate(format: "age > %d", argumentArray: [10])
        // requestDel.predicate = predicateDel
        
        do {
            let arrObj = try context.fetch(requestDel)
            for obj in arrObj as! [NSManagedObject] {
                context.delete(obj)
            }
        } catch {
            print("Failed")
        }
        saveManagedObject(context: context)
    }
}

// MARK: Mocked Facade
private class CoreDataMock: CoreDataModule {
    // TODO
    func fetch(entityName: String) throws -> (NSManagedObjectContext, [NSManagedObject]) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            return (context, result)
        } catch let error {
            throw CoreDataErrorHandler(errorCode: .CANNOT_FETCH_DATA , description: error.localizedDescription)
        }
    }
    
    func createNewMAnagedObject(entityName: String, context: NSManagedObjectContext) -> NSManagedObject? {
        return nil
    }
    
    /// TODO
    
    
    func getManagedObject(forEntityName: String, completion: @escaping(CoreDataErrorHandler? ,NSManagedObjectContext?, NSManagedObject?) -> ()) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        if let entity = NSEntityDescription.entity(forEntityName: forEntityName, in: context) {
        let managedObject = NSManagedObject(entity: entity, insertInto: context)
            completion(nil, context, managedObject)
        }else {
            completion( CoreDataErrorHandler(errorCode: .ENTITY_NOT_FOUND, description: nil), nil, nil)
        }
    }
    
    func saveManagedObject(context: NSManagedObjectContext) {
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func addValueToManagedObject(managedObject: NSManagedObject, key: String, value: Any) { managedObject.setValue(value, forKey: key) }
    
    func fetch(entityName: String) throws -> [NSManagedObject] {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        //request.predicate = NSPredicate(format: "age = %@", "12") // ??
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            return result
        } catch let error {
            throw CoreDataErrorHandler(errorCode: .CANNOT_FETCH_DATA , description: error.localizedDescription)
        }
    }
    
    func getValueFromKeyInEntity(key: String, managedObject: NSManagedObject) -> Any? { return managedObject.value(forKey: key) }
}
 

