//
//  CoreDataErrorHandler.swift
//  Appro-app Modules
//
//  Created by Matt Jacquet on 4/26/21.
//

import Foundation


class CoreDataErrorHandler: Error {
    enum ErrorCode: Int {
        case UNKNOWN = 0
        case CANNOT_FETCH_DATA = 1
        case ENTITY_NOT_FOUND = 2
    }
    
    var errorCode: Int
    var description: String?
    
    init(errorCode: ErrorCode, description: String?) {
        self.errorCode = errorCode.rawValue
        self.description = description
    }
    
    static func logError(_ error: ErrorCode, description: String?) {
        print("CoreDataErrorHandler ERROR \(error)", description ?? "")
    }
}

